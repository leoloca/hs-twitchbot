# bot.py
import os  # for importing env vars for the bot to use
from twitchio.ext import commands
from twitchio.client import Client
from twitchio.webhook import StreamChanged
from web_s import scrape_pos, scrape_rate, scrape_top, scrape_bottom, scrape_bef, scrape_aft, scrape_est
from selenium import webdriver
import sys

chrome_options = webdriver.ChromeOptions()

chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("--no-sandbox")

prefs = {'profile.managed_default_content_settings.images': 2}
chrome_options.add_experimental_option('prefs', prefs)

driver = webdriver.Chrome(chrome_options=chrome_options)


bot = commands.Bot(
    # set up the bot
    irc_token=os.environ['TMI_TOKEN'],
    client_id=os.environ['CLIENT_ID'],
    nick=os.environ['BOT_NICK'],
    prefix=os.environ['BOT_PREFIX'],
    initial_channels=[os.environ['CHANNEL']]
)


@bot.event
async def event_ready():
    'Called once when the bot goes online.'
    print(f"{os.environ['BOT_NICK']} is online!")
    ws = bot._ws  # this is only needed to send messages within event_ready

    pos = scrape_pos(driver, ACCOUNT_NAME)
    if pos == None:
       pos = scrape_pos(driver, ACCOUNT_NAME)

    if pos != None:
        posi = pos[0]
        f = open("initial_position.txt", "w")
        f.write(posi)
        f.close()

        f = open("stream_top.txt", "w")
        f.write(posi)
        f.close()
    else:
        f = open("initial_position.txt", "w")
        f.write("out")
        f.close()

        f = open("stream_top.txt", "w")
        f.write("out")
        f.close()

    await ws.send_privmsg(os.environ['CHANNEL'], f"/me has landed!")


@bot.event
async def event_message(ctx):
    'Runs every time a message is sent in chat.'

    # make sure the bot ignores itself and the streamer
    if ctx.author.name.lower() == os.environ['BOT_NICK'].lower():
        return

    await bot.handle_commands(ctx)

    # await ctx.channel.send(ctx.content)

    if 'murloc' in ctx.content.lower():
        await ctx.channel.send(f"Mrglglrglglglglglglgl!, @{ctx.author.name}!")

    if 'prime' in ctx.content.lower():
        await ctx.channel.send(f"Subscribe using twitch prime, @{ctx.author.name}!")


@bot.command(name='rank')
async def rank(ctx):
    now = None
    new = scrape_pos(driver, ACCOUNT_NAME)
    if new == None:
        new = scrape_pos(driver, ACCOUNT_NAME)

    if new != None:
        now = new[0]

    f = open("initial_position.txt", "r")
    start = f.readline()
    f.close()

    f = open("stream_top.txt", "r")
    top_stream = f.readline()
    f.close()

    f = open("top_position.txt", "r")
    top_all = f.readline()
    f.close()

    if now != None:

        if top_stream == "out":
            top_stream = 201

        if int(now) < int(top_stream):
            f = open("stream_top.txt", "w")
            f.write(now)
            f.close()
            top_stream = now

        if int(now) < int(top_all):
            f = open("top_position.txt", "w")
            f.write(now)
            f.close()
            top_all = now

        ChannelName = os.environ['CHANNEL']
        await ctx.send(f'{ChannelName} rank ({new[2]} MMR): START(stream): {start} - TOP(stream): {top_stream} - TOP(all): {top_all} - NOW: {now}')

    if now == None:
        ChannelName = os.environ['CHANNEL']
        await ctx.send(f'{ChannelName} has not been found (maybe it is not in TOP 200?)')


@bot.command(name='repeat')
async def repeat(ctx):
   await ctx.send(ctx.content[8:])


@bot.command(name='estimate_rank')
async def estimate_rank(ctx):
    number = ctx.content[14:]
    new = scrape_est(driver, number)
    if new == None:
        await ctx.send(f'Estimated rank is less than 200 with {number} MMR')
    if new != None:
        await ctx.send(f'Estimated rank is {new[0]} with {number} MMR')


@bot.command(name='next_up')
async def next(ctx):
    new = scrape_bef(driver, ACCOUNT_NAME)
    if new == None:
        bottom = scrape_bottom(driver)
        await ctx.send(f'Next up:{bottom[1]} is at the bottom of BG with {bottom[2]}')
        await ctx.send(f'Not Found')
    if new != None:
        await ctx.send(f'Next up: {new[1]} rank is {new[0]} with {new[2]} MMR')


@bot.command(name='next_down')
async def bef(ctx):
    new = scrape_aft(driver, ACCOUNT_NAME)
    if new == None:
        bottom = scrape_bottom(driver)
        await ctx.send(f'Next down:{bottom[1]} is at the bottom of BG with {bottom[2]}')
    if new != None:
        await ctx.send(f'Next down: {new[1]} rank is {new[0]} with {new[2]} MMR')


@bot.command(name='find_rank')
async def find_rank(ctx):
    name = ctx.content[11:]
    new = scrape_pos(driver, name)

    if new == None:
        await ctx.send(f'{name} has not been found (maybe it is not in TOP 200?)')
    if new != None:
        await ctx.send(f'{name} rank is {new[0]} with {new[2]} MMR')


@bot.command(name='top_rank')
async def top_rank(ctx):

    top = scrape_top(driver)
    await ctx.send(f'{top[1]} is at the top of BG with {top[2]} MMR')


@bot.command(name='bottom_rank')
async def bottom_rank(ctx):
    bottom = scrape_bottom(driver)
    await ctx.send(f'{bottom[1]} is at the bottom of BG with {bottom[2]} MMR')


@bot.command(name='l3o_commands')
async def commandos(ctx):
    await ctx.send(f'remove_one -- print_list -- register -- rank -- repeat(message) -- estimate_rank(number) -- next_up -- next_down -- find_rank(name) -- top_rank -- bottom_rank')


@bot.command(name='number_one')
async def number_one(ctx):
    ChannelName = os.environ['CHANNEL']
    await ctx.send(f'If {ChannelName} Kappa has million number of fans i am one of them person_raising_hand_tone1. if {ChannelName} has ten fans i am one of them. if {ChannelName} have only one fan and that is me person_raising_hand_tone2person_raising_hand_tone3person_raising_hand_tone4. if {ChannelName} has no fans, that means i am no more on the earth cry. if world against the {ChannelName}, i am against the world xearth_africacomet. i love #{ChannelName} till my last breath.')


@bot.command(name='clear_list')
async def empty_list(ctx):
    if ((ctx.author.is_mod) or (ctx.author.name.lower() == "damo93hs")):
        file = open("prenotation.txt", "w")
        file.close()
        await ctx.send(f'List of prenotations cleared')
    else:
        await ctx.send(f'Only mods and channel owner can clear the list')


@bot.command(name='clear_list_sub')
async def empty_list_sub(ctx):
    if ((ctx.author.is_mod) or (ctx.author.name.lower() == "damo93hs")):
        file = open("prenotation_sub.txt", "w")
        file.close()
        await ctx.send(f'List of prenotations cleared')
    else:
        await ctx.send(f'Only mods and channel owner can clear the list')


@bot.command(name='register')
async def reg(ctx):

    my_file = open("prenotation.txt", "r")
    content_list = my_file.readlines()
    total_line_count = len(content_list)

    if(total_line_count < 7):
        with open('prenotation.txt', 'a') as file:
            name=ctx.author.name.lower()+"\n"
            if(name not in content_list):
                file.write(ctx.author.name.lower())
                file.write("\n")
                await ctx.send(f'{ctx.author.name.lower()} has been registered!({total_line_count+1}/7)')
            else:
                await ctx.send(f'{ctx.author.name.lower()} is already in the list!')

    else:
        await ctx.send(f'{ctx.author.name.lower()} sorry the list is full')


@bot.command(name='register_sub')
async def reg_sub(ctx):

    if ((ctx.author.is_subscriber==1) or (ctx.author.name.lower() == "damo93hs")):
        my_file = open("prenotation_sub.txt", "r")
        content_list = my_file.readlines()
        total_line_count = len(content_list)

        if(total_line_count < 7):
            with open('prenotation_sub.txt', 'a') as file:
                name = ctx.author.name.lower()+"\n"
                if(name not in content_list):
                    file.write(ctx.author.name.lower())
                    file.write("\n")
                    await ctx.send(f'{ctx.author.name.lower()} has been registered!({total_line_count+1}/7)')
                else:
                    await ctx.send(f'{ctx.author.name.lower()} is already in the list!')

        else:
            await ctx.send(f'{ctx.author.name.lower()} sorry the list is full')
    else:
        await ctx.send(f'{ctx.author.name.lower()} you are not a sub!! This list is sub only')


@bot.command(name='print_list_sub')
async def print_list_sub(ctx):
    my_file = open("prenotation_sub.txt", "r")
    content_list = my_file.readlines()
    total_line_count = len(content_list)
    st = ""
    for sp in content_list:
        st = st + sp + ";"

    st = st + f" ({total_line_count}/7)"
    await ctx.send(st)


@bot.command(name='print_list')
async def print_list(ctx):
    my_file = open("prenotation.txt", "r")
    content_list = my_file.readlines()
    total_line_count = len(content_list)
    st = ""
    for sp in content_list:
        st = st + sp + ";"

    st = st + f" ({total_line_count}/7)"
    await ctx.send(st)


@bot.command(name='remove_one_sub')
async def remove_one_sub(ctx):
    if ((ctx.author.is_mod) or (ctx.author.name.lower() == "damo93hs")):
        my_file = open("prenotation_sub.txt", "r")
        content_list = my_file.readlines()
        #total_line_count = len(content_list)    
        lista=[]
        found=0
        name = (ctx.content[12:]).lower() +"\n"

    
        for sp in content_list:
            if(sp != name ):
                lista.append(sp)
            else:
                found=1

        my_file.close()
        file = open("prenotation_sub.txt", "w")
        file.close()
        file = open("prenotation_sub.txt", "w")
    
        for sp in lista:
            file.write(sp)
            #file.write("\n")  

        if found==1:
            await ctx.send(f'{name} removed!')
        else:
            await ctx.send(f'{name} not in the list!')
    else:
        await ctx.send(f'Only mods and channel owner can use this command')


@bot.command(name='remove_one')
async def remove_one(ctx):
    if ((ctx.author.is_mod) or (ctx.author.name.lower() == "damo93hs")):
        my_file = open("prenotation.txt", "r")
        content_list = my_file.readlines()
        #total_line_count = len(content_list)
        lista = []
        found = 0
        name = (ctx.content[12:]).lower() + "\n"

        for sp in content_list:
            if(sp != name):
                lista.append(sp)
            else:
                found = 1

        my_file.close()
        file = open("prenotation.txt", "w")
        file.close()
        file = open("prenotation.txt", "w")

        for sp in lista:
            file.write(sp)
            #file.write("\n")

        if found == 1:
            await ctx.send(f'{name} removed!')
        else:
            await ctx.send(f'{name} not in the list!')
    else:
        await ctx.send(f'Only mods and channel owner can use this command')


@bot.command(name='start')
async def start_rank(ctx):

    if ((ctx.author.is_mod) or (ctx.author.name.lower() == "damo93hs")):

        pos = scrape_pos(driver, ACCOUNT_NAME)
        if pos == None:
            pos = scrape_pos(driver, ACCOUNT_NAME)

        if pos != None:
            posi = pos[0]
            f = open("initial_position.txt", "w")
            f.write(posi)
            f.close()

            f = open("stream_top.txt", "w")
            f.write(posi)
            f.close()
        else:
            f = open("initial_position.txt", "w")
            f.write("out")
            f.close()

            f = open("stream_top.txt", "w")
            f.write("out")
            f.close()

        await ctx.send(f'The starting rank has been set')
    else:
        await ctx.send(f'Only mods and channel owner can modify starting rank')


if __name__ == "__main__":
    args = sys.argv
    #ACCOUNT_NAME = args[1]
    ACCOUNT_NAME = "DAMO93"
    bot.run()
