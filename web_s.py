from selenium import webdriver
import lxml
import time

URLS = ["https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=2",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=3",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=4",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=5",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=6",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=7",
        "https://playhearthstone.com/en-gb/community/leaderboards?region=EU&leaderboardId=BG&page=8"]


def scrape_pos(driver, name):

    for url in URLS:
        driver.get(url)

        content = driver.find_elements_by_class_name("row")
        while(content == []):
            content = driver.find_elements_by_class_name("row")
        for row in content:
            rr = (row.text).split('\n')
            batt = rr[1]

            if(batt == name):
                return rr


def scrape_bef(driver, name):

    for url in URLS:
        driver.get(url)

        content = driver.find_elements_by_class_name("row")
        while(content == []):
            content = driver.find_elements_by_class_name("row")
        for row in content:
            rr = (row.text).split('\n')
            batt = rr[1]

            if(batt == name):
                return old

            old = rr


def scrape_aft(driver, name):

    el = None
    for url in URLS:
        driver.get(url)

        content = driver.find_elements_by_class_name("row")
        while(content == []):
            content = driver.find_elements_by_class_name("row")
        for row in content:
            rr = (row.text).split('\n')
            batt = rr[1]
            if el == 1:
                return rr
            if(batt == name):
                el = 1


def scrape_rate(driver, name):

    for url in URLS:
        driver.get(url)

        content = driver.find_elements_by_class_name("row")
        while(content == []):
            content = driver.find_elements_by_class_name("row")
        for row in content:
            rr = (row.text).split('\n')
            pos = rr[0]
            batt = rr[1]
            rating = rr[2]
            if(batt == name):
                return(rating)


def scrape_top(driver):

    url = URLS[0]

    driver.get(url)

    content = driver.find_elements_by_class_name("row")
    while(content == []):
        content = driver.find_elements_by_class_name("row")
    row = content[0]
    rr = (row.text).split('\n')

    return rr


def scrape_bottom(driver):

    url = URLS[-1]

    driver.get(url)

    content = driver.find_elements_by_class_name("row")
    while(content == []):
        content = driver.find_elements_by_class_name("row")

    row = content[-1]
    rr = (row.text).split('\n')

    return rr


def scrape_est(driver, number):

    for url in URLS:
        driver.get(url)

        content = driver.find_elements_by_class_name("row")
        while(content == []):
            content = driver.find_elements_by_class_name("row")
        for row in content:
            rr = (row.text).split('\n')
            rating = rr[2]
            if(int(rating) < int(number)):
                return old
            old = rr


if __name__ == "__main__":

    driver = webdriver.Chrome(
        "C:/Users/Leonardo/Desktop/twitchbot/chromedriver")
    scrape_pos(driver, "test")
